# twtxt.tcl

This repository hosts tools for the [twtxt](https://wiki.tcl-lang.org/page/twtxt) feed format.

It contains a graphical application and two command-line tools:
  * A basic cross-platform GUI twtxt feed reader ([`app/timeline.tcl`](app/timeline.tcl));
  * A command line utility for extracting all mentions from a twtxt feed ([`app/mentions.tcl`](app/mentions.tcl)).
  * A command line utility for converting an RSS feed to a twtxt feed ([`app/rss2twtxt.tcl`](app/rss2twtxt.tcl)).

It also includes Tcl libraries for
  * Parsing a twtxt feed ([`lib/twtxt/twtxt.tcl`](lib/twtxt/twtxt.tcl));
  * Displaying emoji in a Tk text widget in Tcl/Tk builds limited to the basic multilingual plane ([`lib/emoji-hack/emoji-hack.tcl`](lib/emoji-hack/emoji-hack.tcl));
  * Reading files in a uniform manner form the local filesystem, HTTP(S), Gopher, and Gemini. ([`lib/read-url/read-url.tcl`](lib/read-url/read-url.tcl)).


## Feed reader screenshot

![A screenshot of app/timeline.tcl showing some bots](screenshot.png)


## Dependencies

### Programs

#### Feed reader (`app/timeline.tcl`)

  * Tcl 8.6 (preferably 8.6.10 or later)
  * Tk
  * The SQLite3 extension for Tcl
  * [TclTLS](https://wiki.tcl-lang.org/page/tls) (\*nix) or [TWAPI](https://wiki.tcl-lang.org/page/TWAPI) (Windows)

On Debian/Ubuntu you can install all of the above with the command

```sh
sudo apt install libsqlite3-tcl tcl tcl-tls tk
```

#### `app/mentions.tcl`

  * Tcl 8.6 or later
  * TclTLS or TWAPI

#### `app/rss2twtxt.tcl`

  * Tcl 8.6 or later
  * Tcllib 1.15 or later
  * TclTLS or TWAPI
  * [tDOM](https://wiki.tcl-lang.org/page/tDOM)

```sh
sudo apt install tcl tcllib tcl-tls tdom
```

### Libraries

#### `lib/twtxt/twtxt.tcl`

  * Tcl 8.5 or later.  Jim Tcl works conditionally and may parse timestamps incorrectly.

#### `lib/emoji-hack/emoji-hack.tcl`

  * Tcl 8.6.10 or later

#### `lib/read-url/read-url.tcl`

  * Tcl 8.6 or later
  * TclTLS or TWAPI for accessing HTTPS and Gemini


## License

MIT.  See the file [LICENSE](LICENSE).

[vendor/twemoji](vendor/twemoji): Copyright 2019 Twitter, Inc and other contributors.  CC-BY 4.0 license.
