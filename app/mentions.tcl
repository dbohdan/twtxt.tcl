#! /usr/bin/env tclsh
# Copyright (c) 2020-2021, 2024 D. Bohdan.
# License: MIT.

package require Tcl 8.6 9

foreach subdir {lib vendor} {
    lappend auto_path [file join [file dirname [info script]] .. $subdir]
}
package require read-url
package require twtxt


proc mentions {{path {}}} {
    read-url::init-tls

    set anonymous [dict create]
    set byUser [dict create]

    set feed [if {$path eq {}} {
        read stdin
    } else {
        read-url::consume-result [read-url::request $path]
    }]

    # Deduplicate.
    foreach {user url} [twtxt::mentions $feed] {
        if {$user eq {}} {
            dict set anonymous $url 1
            continue
        }

        dict set byUser $user $url
    }

    foreach url [dict keys $anonymous] {
        puts [list ? $url]
    }
    dict for {user url} $byUser {
        puts [list $user $url]
    }

    exit 0
}


mentions {*}$argv
