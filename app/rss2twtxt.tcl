#! /usr/bin/env tclsh
# Inspired by https://github.com/prologic/rss2twtxt but not based on its code.
# Copyright (c) 2020-2021, 2024 D. Bohdan.
# License: MIT.

package require Tcl 8.6 9

package require tdom
package require clock::rfc2822  ;# Tcllib 1.15 or later.

foreach subdir {lib vendor} {
    lappend auto_path [file join [file dirname [info script]] .. $subdir]
}
package require read-url
package require twtxt


proc rss2twtxt {{path {}}} {
    read-url::init-tls

    set feed [if {$path eq {}} {
        read stdin
    } else {
        read-url::consume-result [read-url::request $path]
    }]

    set doc [dom parse $feed]
    set root [$doc documentElement]
    foreach item [$root selectNodes //item] {
        foreach field {link pubDate title} {
            set $field [[$item selectNodes .//$field] asText]
        }

        set t [::clock::rfc2822 parse_date $pubDate]
        set timestamp [clock format $t \
            -format %Y-%m-%dT%H:%M:%SZ \
            -timezone :UTC \
        ]
        puts "$timestamp\t[string map [list \n { }] $title] <$link>"
    }

    exit 0
}


rss2twtxt {*}$argv
