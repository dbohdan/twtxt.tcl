#! /usr/bin/env tclsh
# twtxt feed reader.
# Copyright (c) 2020-2021, 2024 D. Bohdan
# License: MIT.

package require Tcl 8.6 9
package require TclOO
set argv [list -- {*}$argv]
package require Tk

# Vendored dependencies.
foreach subdir {lib vendor} {
    lappend auto_path [file join [file dirname [info script]] .. $subdir]
}
package require emoji-hack
package require read-url
package require twtxt

# System dependencies.
package require sqlite3
try {
    package require twapi_crypto
    set read-url::tlsSocketScript [list ::twapi::tls_socket]
} on error _ {
    package require tls
    set read-url::tlsSocketScript [list ::tls::socket -autoservername true]
}
http::register https 443 ${read-url::tlsSocketScript}


namespace eval tl {
    variable cli {
        hide feeds
        info {
            char-image-dir {emoji image directory}
            emoji-scaling  {emoji image scaling (multiplier and divisor)}
            feed-list      {file from which to read a list of names and\
                            feed URLs}
            font-size      {main feed font size}
            interval       {feed refresh interval (seconds)}
            log-level      {how detailed the program's log should be\
                            ("nothing" to disable)}
            time-zone      {time zone for timestamps (local if empty)}
        }
    }
    variable config
    variable version 0.1.0

    array set config {
        emoji-scaling {2 5}
        feed-list {}
        feeds {}
        font-size 18
        interval 600
        log-level info
        time-zone {}
    }
    set config(char-image-dir) [file join \
        [file dirname [info script]] \
        ../vendor/twemoji/72x72 \
    ]
}
namespace eval tl::log {
    variable config
    array set config {
        channel stderr
        format {== $timestamp ($level) [list $message]}
        gmt 1
        level info
        levels {
            nothing -1
            emerg 0
            alert 1
            crit 2
            err 3
            warning 4
            notice 5
            info 6
            debug 7
            discard 99
        }
        timestamp {%Y-%m-%d %H:%M:%S UTC}
    }
}
namespace eval tl::view {
    upvar [namespace parent]::config config
}


### tl ###

proc tl::main argv {
    variable config

    rename ::send {}

    cli $argv
    if {$config(feed-list) ne {}} {
        set token [read-url::request $config(feed-list)]

        set config(feeds) [concat \
            [parse-feed-list [read-url::consume-result $token]] \
            $config(feeds) \
        ]
    }

    interp bgerror {} [list apply [list {msg opts} {
        log::log err $msg
    } [namespace current]]]

    sqlite3 db :memory:

    db eval {
        CREATE TABLE statuses (
            id INTEGER PRIMARY KEY,
            timestamp INTEGER,
            user TEXT,
            text TEXT
        );

        CREATE TABLE retrieved (
            id INTEGER PRIMARY KEY,
            FOREIGN KEY (id) REFERENCES statuses(id)
        );

        CREATE INDEX idx_statuses
        ON statuses (timestamp, user, text);
    }

    wm title . Timeline
    set view [View new .view [array get config]]
    grid [$view path] -sticky nsew
    grid columnconfigure . [$view path] -weight 1
    grid rowconfigure . [$view path] -weight 1

    coroutine loop apply [list view {
        variable config

        set first true
        while true {
            log::log debug {coroutine loop}

            set requests {}
            foreach {user path} $config(feeds) {
                set token [read-url::request \
                    $path \
                    false \
                    [list [namespace current]::log::log info] \
                ]

                lappend requests $user $path $token
            }

            foreach {user path token} $requests {
                try {
                    set contents [read-url::consume-result $token]
                    load-feed $user $contents
                } on error e {
                    log::log err [list can't access $path : $e]
                }
            }

            set atBottom [$view at-bottom?]
            $view populate [tl::new-statuses]
            if {$atBottom || $first} { $view move-to 1 }
            set first false

            after [expr {$config(interval) * 1000}] [info coroutine]
            yield
        }
    } [namespace current]] $view
}


proc tl::cli argv {
    variable config

    if {$argv in {{} -h -help --help /?}} {
        set help [expr {$argv ne {}}]
        usage $help
        exit [expr {!$help}]
    }

    while {[regexp ^- [lindex $argv 0]]} {
        if {[lindex $argv 0] eq {--}} {
            set argv [lrange $argv 1 end]
            break
        }

        set argv [lassign $argv key value]
        regsub ^--? $key {} key

        set config($key) $value
    }

    if {([llength $argv] == 0 && $config(feed-list) eq {})
        || [llength $argv] % 2 == 1} {
        usage
        exit 1
    }

    set config(feeds) $argv
    set log::config(level) $config(log-level)

    return
}


proc tl::usage {{long false}} {
    variable cli
    variable config

    set me [file tail [info script]]
    puts stderr Usage:
    puts stderr "    $me \[--option value\] \[--\] username feed\
                 \[username feed ...\]"

    if {!$long} return

    set maxLen 22
    puts stderr Options:
    foreach key [lsort [array names config]] {
        if {$key in [dict get $cli hide]} continue

        set value $config($key)
        if {$value eq {} || [regexp {\s} $value]} {
            set value '$value'
        }

        set kv "--$key $value"
        if {[string length $kv] > $maxLen} {
            append kv "\n    [format "%${maxLen}s" {}]"
        }
        puts stderr [format "    %-${maxLen}s %s" \
            $kv \
            [dict get $cli info $key] \
        ]
    }

    puts stderr "\nEach feed can be either a filesystem path or an HTTP(S)\
                 URL."
}


proc tl::parse-feed-list feedList {
    set feeds {}
    foreach line [split [string trimright $feedList] \n] {
        if {[regexp {^\s*(?:$|#)} $line]} continue

        lappend feeds {*}$line
    }

    return $feeds
}


proc tl::load-feed {user contents} {
    set parsed [twtxt::parse-feed $contents]

    db transaction {
        foreach {timestamp text} [dict get $parsed statuses] {
            db eval {
                INSERT INTO statuses (timestamp, user, text)
                SELECT :timestamp, :user, :text
                WHERE NOT EXISTS(
                    SELECT 1 FROM statuses
                    WHERE timestamp = :timestamp AND
                          user = :user AND
                          text = :text
                );
            }
        }
    }
}


proc tl::new-statuses {} {
    set rows {}

    db eval {
        SELECT s.id, s.timestamp, s.user, s.text
        FROM statuses AS s
        LEFT JOIN retrieved AS r ON
            s.id = r.id
        WHERE r.id IS NULL
        ORDER BY s.timestamp ASC;
    } row {
        lappend rows [array get row]
    }
    unset row

    db transaction {
        foreach row $rows {
            set id [dict get $row id]

            db eval {
                INSERT INTO retrieved (id)
                VALUES (:id);
            }
        }
    }

    return $rows
}


### tl::log ###

proc tl::log::level-number level {
    variable config

    if {![dict exists $config(levels) $level]} {
        return \
            -code error \
            -errorcode {LOG BAD-LEVEL} \
            [list unknown log level $level] \
    }

    return [dict get $config(levels) $level]
}


proc tl::log::log {level message} {
    variable config

    if {[level-number $config(level)] < [level-number $level]} {
        return
    }

    set timestamp [clock format \
        [clock seconds] \
        -format $config(timestamp) \
        -gmt $config(gmt) \
    ]

    puts $config(channel) [subst $config(format)]
}


### View class ###

oo::class create tl::View {
    variable _charImageDir
    variable _emojiScaling
    variable _path
    variable _searchMode
    variable _timeZone

    constructor {path config} {
        set _path $path

        set fontSize [dict get $config font-size]
        set _charImageDir [dict get $config char-image-dir]
        set _emojiScaling [dict get $config emoji-scaling]
        set _timeZone [dict get $config time-zone]

        set _searchMode -regexp

        # Create the fonts.
        font create TlViewFont \
            -family {DejaVu Sans} \
            -size $fontSize \

        font create TlViewFontUser \
            {*}[font configure TlViewFont] \
            -weight bold \

        font create TlViewFontTime \
            {*}[font configure TlViewFont] \
            -size [expr { round($fontSize * 7 / 9) }] \

        font create TlViewFontPadding \
            {*}[font configure TlViewFont] \
            -size [expr { round($fontSize * 1 / 2) }] \

        # Create the text widget that will display statuses.
        ttk::frame $path
        text $path.text \
            -background #f0f0f0 \
            -font TlViewFont \
            -foreground #000000 \
            -insertwidth 0 \
            -wrap word \
            -yscrollcommand [list $path.scroll set] \

        my make-text-read-only ::$path.text

        bind $path.text <Up> [list $path.text yview scroll -1 units]
        bind $path.text <Down> [list $path.text yview scroll 1 units]

        $path.text tag configure even -background #e0e0e0
        $path.text tag configure odd -background #f0f0f0
        $path.text tag configure padding -font TlViewFontPadding
        $path.text tag configure search -background yellow
        $path.text tag configure time -font TlViewFontTime
        $path.text tag configure user -font TlViewFontUser

        ttk::scrollbar $path.scroll -command [list $path.text yview]

        # Create the search bar.
        image create photo [self namespace]::TlImageUpArrow -data {
            iVBORw0KGgoAAAANSUhEUgAAAAwAAAAHAQMAAAAGfD5nAAAABlBMVEWVelUAAACnfTnBAA
            AAAXRSTlMAQObYZgAAAB1JREFUCNdjYGNg4GdgkG9gsDzAUPCA4UEBwwEDACUhBT4tPZiO
            AAAAAElFTkSuQmCC
        }
        image create photo [self namespace]::TlImageDownArrow -data {
            iVBORw0KGgoAAAANSUhEUgAAAAwAAAAHAQMAAAAGfD5nAAAABlBMVEUAAAAAAAClZ7nPAA
            AAAXRSTlMAQObYZgAAAB1JREFUCNdjOGDA8KCAoeABg+UBBvkGBn4GBjYGAEjHBT4/q+Lr
            AAAAAElFTkSuQmCC
        }

        ttk::frame $path.search -padding 5p

        grid [ttk::label $path.search.label -font TlViewFont -text Search] \
            -row 0 -column 0 -padx {5p 10p}

        grid [ttk::entry $path.search.query -font TlViewFont] \
            -row 0 -column 1 -sticky nsew
        grid columnconfigure $path.search $path.search.query -weight 1
        grid rowconfigure $path.search $path.search.query -weight 1

        grid [ttk::button $path.search.back \
            -command [list [self] search -] \
            -image [self namespace]::TlImageUpArrow \
            -width 2 \
        ] -row 0 -column 2 -sticky ns

        grid [ttk::button $path.search.forward \
            -command [list [self] search +] \
            -image [self namespace]::TlImageDownArrow \
            -width 2 \
        ] -row 0 -column 3 -sticky ns

        grid [ttk::checkbutton $path.search.regexp \
            -offvalue -exact \
            -onvalue -regexp \
            -text Regexp \
            -variable [self namespace]::_searchMode \
        ] -row 0 -column 4 -padx {10p 5p} -sticky ns

        bind $path.search.query \
            <Shift-KeyPress-Return> \
            [list [self] search -] \

        bind $path.search.query \
            <KeyPress-Return> \
            [list [self] search +] \

        foreach {seq clear} {<Control-KeyPress-f> 0 <KeyPress-slash> 1} {
            bind [winfo toplevel $path] \
                $seq \
                [list [self] focus-search $clear] \
        }

        # Place everything on the frame.
        grid $path.text $path.scroll -sticky nsew
        grid $path.search -columnspan 2 -sticky nsew
        grid columnconfigure $path $path.text -weight 1
        grid rowconfigure $path $path.text -weight 1
    }


    method at-bottom? {} {
        return [expr {
            [lindex [$_path.text yview] 1] == 1
        }]
    }


    method focus-search clear {
        set widget $_path.search.query
        if {[focus] eq $widget} return

        focus $widget
        if {$clear} {
            $widget delete 0 end
        } else {
            $widget selection range 0 end
        }
    }


    method make-text-read-only cmd {
        rename $cmd $cmd.internal
        proc $cmd args [format {
            if {[lindex $args 0] in {insert delete}} return

            return [%s.internal {*}$args]
        } $cmd]
    }


    method move-to fraction {
        $_path.text yview moveto $fraction
    }


    method path {} {
        return $_path
    }


    method populate statuses {
        set internal $_path.text.internal

        set clockOpts {}
        if {$_timeZone ne {}} {
            lappend clockOpts -timezone $_timeZone
        }

        set lastStatus [$internal tag prevrange status end]
        set i [if {$lastStatus eq {}} {
            lindex 0
        } else {
            set lastChar [list [lindex $lastStatus 1] - 1 char]
            expr {{odd} in [$internal tag names $lastChar]}
        }]

        foreach status $statuses {
            set time [clock format \
                [dict get $status timestamp] \
                -format {%Y-%m-%d %H:%M:%S %z} \
                {*}$clockOpts \
            ]

            set tags "status [expr {$i % 2 ? {even} : {odd}}]"

            $internal insert end \n "$tags padding"

            emoji-hack::insert-with-char-images \
                $_charImageDir \
                $internal \
                end \
                [dict get $status user] \
                "$tags user" \
                $_emojiScaling \

            $internal insert end { } $tags
            $internal insert end ($time): "$tags time"

            set text [dict get $status text]
            set break [expr {
                [string first \n $text] > -1 ? "\n" : { }
            }]

            $internal insert end $break $tags
            emoji-hack::insert-with-char-images \
                $_charImageDir \
                $internal \
                end \
                $text\n \
                "$tags message" \
                $_emojiScaling \

            $internal insert end \n "$tags padding"

            incr i
        }

        $_path.text sync
    }


    method search direction {
        if {$direction eq {+}} {
            lassign [$_path.text tag ranges search] _ start
            if {$start eq {}} { set start @0,0 }
            set dirFlag -forwards
        } elseif {$direction eq {-}} {
            lassign [$_path.text tag ranges search] start
            if {$start eq {}} { set start end }
            set dirFlag -backwards
        } else {
            error [list unknown direction $direction]
        }

        $_path.text tag remove search 0.0 end

        set query [$_path.search.query get]
        set index [$_path.text search \
            -count ::count \
            $dirFlag \
            -nocase \
            $_searchMode \
            -- \
            $query \
            $start \
        ]

        if {$index ne {}} {
            $_path.text tag add search $index "$index + $::count chars"
            $_path.text yview -pickplace $index
        }
    }
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    tl::main $argv
}
