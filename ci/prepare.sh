#! /bin/sh

set -e

cd "$(dirname "$(readlink -f "$0")")" || exit
echo "running $(basename "$0") in $(pwd)"

wget -O tclkit-8.5.19 http://kitcreator.rkeene.org/kits/a3bc8aaf59c00e166d2be6f1e59b6e43eaf0870f/tclkit
wget -O tclkit-8.6.10 http://kitcreator.rkeene.org/kits/1027aea8fcb581ff672bf7800aabc268f50c6150/tclkit
wget -O jimsh-0.79    https://github.com/dbohdan/jimsh-static/releases/download/v2-0.79-5df766d513/jimsh-0.79-5df766d513-amd64
wget -O jimsh-0.80    https://github.com/dbohdan/jimsh-static/releases/download/v3-0.80-fb923fab4f/jimsh-0.80-fb923fab4f-amd64

echo 'fd1fb01ff5ad756b1fd2da31a63b2d562c63bb2182858660ac5f734cf717629c  tclkit-8.5.19' > checksum
echo 'a6f52deb549d019b10dd21ed2327c3359656c891bdf63b4ea4cc853ad4c065d1  tclkit-8.6.10' >> checksum
echo '2ed762acdb2b0bb3c84d4446388abec9b9074b00f0f5d92633358259f9162cf4  jimsh-0.79' >> checksum
echo '3d87bc22fd4be3032e4235e6ead95a814801328d4ed19e87b19d556c448d7158  jimsh-0.80' >> checksum
sha256sum -c checksum && rm checksum

chmod +x tclkit* jimsh*
