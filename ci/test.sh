#! /bin/sh

set -e

cd "$(dirname "$(readlink -f "$0")")" || exit
echo "running $(basename "$0") in $(pwd)"

run() {
    printf "\n===== %s\n" "$*"
    "$@"
}

run ./tclkit-8.6.10 ../lib/emoji-hack/emoji-hack.tcl

run ./tclkit-8.6.10 ../lib/read-url/read-url.tcl -constraints internet

run ./tclkit-8.5.19 ../lib/twtxt/twtxt.tcl
run ./tclkit-8.6.10 ../lib/twtxt/twtxt.tcl
run ./jimsh-0.79    ../lib/twtxt/twtxt.tcl || true
run ./jimsh-0.80    ../lib/twtxt/twtxt.tcl || true

run ./tclkit-8.6.10 ../app/mentions.tcl feed.txt

run ./tclkit-8.6.10 ../app/rss2twtxt.tcl timeline.rss
