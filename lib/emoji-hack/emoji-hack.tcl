#! /usr/bin/env tclsh
# A library for working with Unicode characters beyond the basic multilingual
# plane in Tcl builds that do not natively support them and for rendering
# these characters as images in a Tk [text] widget.
#
# Copyright (c) 2020-2021 D. Bohdan.
# License: MIT.

package require Tcl 8.6.10-10

namespace eval emoji-hack { variable version 0.1.1 }

# Return 1 if Tcl is built with TCL_UTF_MAX > 4.  We do not need this hack if
# it is.
proc emoji-hack::higher-plane-build? {} {
    return [expr {
        [string length [encoding convertfrom utf-8 \xF0\x9F\xA4\x94]] == 1
    }]
}

# Take a string that may contain surrogate pairs and convert these pairs into
# character codes.  Returns a list
# {preceding-text-1 char-code-1 ?preceding-text-2 char-code-2 ...?}.  The
# preceding text and the character code may be an empty string.
proc emoji-hack::surrogates s {
    if {$s eq {}} { return {} }

    set result {}

    foreach {_ before high low} [regexp \
        -all \
        -inline \
        {([^\uD800-\uDBFF]*)(?:([\uD800-\uDBFF])([\uDC00-\uDFFF]))?} \
        $s \
    ] {
        set code {}
        if {$high ne {}} {
            set codeH [scan $high %c]
            set codeL [scan $low %c]
            set code [expr {
                0x10000 + ($codeH - 0xD800)*0x400 + $codeL - 0xDC00
            }]
        }
        lappend result $before $code
    }

    return $result
}

# Crudely scale a Tk photo image by the factor of $zoom/$subsample.
proc emoji-hack::scale-image {zoom subsample src dest} {
    set temp [image create photo]
    if {[info commands $dest] ne $dest} {
        image create photo $dest
    }

    $temp copy $src -zoom $zoom $zoom
    $dest copy $temp -subsample $subsample $subsample

    image delete $temp
}

# Return the Tk image representing the Unicode character $code.  If necessary,
# create the Tk images first from a file in $charImageDir.
proc emoji-hack::char-image {charImageDir code {scaling {1 1}}} {
    set cmd [namespace current]::unicode-char-[format %x $code]
    if {[info commands $cmd] eq $cmd} { return $cmd }

    set file [file join $charImageDir [format %x $code]].png
    if {![file exists $file]} { return {} }

    set temp [image create photo -file $file]
    scale-image {*}$scaling $temp $cmd
    image delete $temp

    return $cmd
}

# Insert text $s into text widget $path with characters represented as images
# if necessary.
proc emoji-hack::insert-with-char-images {
    charImageDir
    path
    index
    s
    {tags {}}
    {scaling {1 1}}
} {
    if {[higher-plane-build?]} {
        $path insert $index $s $tags
        return
    }

    foreach {before code} [surrogates $s] {
        $path insert $index $before $tags
        if {$code ne {}} {
            set img [char-image $charImageDir $code $scaling]
            if {$img eq {}} {
                $path insert $index \uFFFD
            } else {
                $path image create $index -align center -image $img
            }
        }
    }
}

proc emoji-hack::run-tests {} {
    package require tcltest


    tcltest::test surrogates-1.1 {} -body {
        surrogates {}
    } -result {}

    tcltest::test surrogates-1.2 {} -body {
        surrogates {Hello, world!}
    } -result {{Hello, world!} {}}

    tcltest::test surrogates-1.3 {} -body {
        surrogates "thnk [encoding convertfrom utf-8 \xF0\x9F\xA4\x94] diff"
    } -result {{thnk } 129300 { diff} {}}

    tcltest::test surrogates-1.4 {} -body {
        surrogates [encoding convertfrom \
            utf-8 \
            "\xF0\x9F\xA4\x97 <3 \xF0\x9F\x98\x8D" \
        ]
    } -result [list {} 129303 { <3 } 128525]

    tcltest::test surrogates-1.5 {} -body {
        surrogates [encoding convertfrom utf-8 \xF0\x9F\xA4\x97]
    } -result {{} 129303}


    # Exit with a nonzero status if there are failed tests.
    set failed [expr {$tcltest::numTests(Failed) > 0}]

    tcltest::cleanupTests
    if {$failed} {
        return 1
    }

    return 0
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    exit [emoji-hack::run-tests]
}

package provide emoji-hack 0
