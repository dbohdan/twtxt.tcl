#! /usr/bin/env tclsh
# Copyright (c) 2020-2021, 2024 D. Bohdan.
# License: MIT.

package require Tcl 8.6 9

package require http
# To request HTTPS URLs with this library, you must first set up TLS support in
# the package "http".  To request Gemini URLs, you must set the variable
# read-url::tlsSocketScript.  See [read-url::run-tests] for an example of how
# to do both.

namespace eval read-url {
    variable counter 0
    variable results; array set results {}
    variable script [info script]
    variable tokens; array set tokens {}
    variable timeout 60000
    variable tlsSocketScript
    variable version 0.5.0
}
namespace eval read-url::parse {}
namespace eval read-url::protocol {
    namespace path [namespace parent]
}
namespace eval read-url::tests {
    namespace path [namespace parent]
}


proc read-url::request {url {binary false} {logScript list}} {
    set token [create-token]

    set protocol [switch -regexp -- $url {
        ^https?://  { lindex http }
        ^gemini://  { lindex gemini }
        ^gopher://  { lindex gopher }
        default     { lindex filesystem }
    }]

    if {$protocol eq {filesystem}} {
        {*}$logScript [list reading $url]
    } else {
        {*}$logScript [list downloading $url]
    }

    try {
        protocol::$protocol $token $url $binary
    } on error {e opts} {
        set-result $token $e $opts
    }

    return $token
}


proc read-url::consume-result token {
    variable results

    consume-token $token

    if {![ready? $token]} {
        vwait [namespace current]::results($token)
    }

    lassign $results($token) value options
    unset results($token)

    return {*}$options $value
}


proc read-url::ready? token {
    info exists [namespace current]::results($token)
}


proc read-url::set-result {token value {options {}}} {
    variable results

    set results($token) [list $value $options]
    return $token
}


proc read-url::consume-token token {
    variable tokens

    if {![info exists tokens($token)]} {
        error [list unknown token $token]
    }
    unset tokens($token)

    return $token
}


proc read-url::create-token {} {
    variable counter
    variable tokens

    set token read-url-[format %06u $counter]
    set tokens($token) 1

    incr counter
    return $token
}


proc read-url::after-unset {varName script} {
    set func [list apply [list args $script]]
    uplevel 1 [list trace add variable $varName unset $func]
}


proc read-url::protocol::filesystem {token url binary} {
    regsub ^file:// $url {} url

    set ch [open $url]
    after-unset ch [list close $ch]
    if {$binary} {
        chan configure $ch -translation binary
    } else {
        chan configure $ch -encoding utf-8
    }

    set-result $token [read $ch]
}


# Based on the Gemini spec v0.14.2.
proc read-url::protocol::gemini {token url binary} {
    variable tlsSocketScript

    set p [parse::gemini-url $url]
    if {$p eq {}} {
        return \
            -code error \
            -errorcode {READ-URL GEMINI URL} \
            [list can't parse URL $url]
    }
    lassign $p host port

    set ch [{*}$tlsSocketScript $host $port]
    after-unset ch [list close $ch]
    if {$binary} {
        chan configure $ch -translation binary
    } else {
        chan configure $ch -encoding utf-8
    }

    puts -nonewline $ch $url\n
    flush $ch
    gets $ch status
    if {[lindex $status 0] != 20} {
        return \
            -code error \
            -errorcode {READ-URL GEMINI REMOTE} \
            [string trim $status]
    }

    set-result $token [read $ch]
}


proc read-url::protocol::gopher {token url binary} {
    set p [parse::gopher-url $url]
    if {$p eq {}} {
        return \
            -code error \
            -errorcode {READ-URL GOPHER URL} \
            [list can't parse URL $url]
    }
    lassign $p host port type path
    if {$type == 9} { set binary true }

    set ch [socket $host $port]
    after-unset ch [list close $ch]
    if {$binary} {
        chan configure $ch -translation binary
    } else {
        chan configure $ch -encoding utf-8
    }

    puts -nonewline $ch /$path\n
    flush $ch
    set data [read $ch]
    if {[regexp ^Error $data]} {
        return \
            -code error \
            -errorcode {READ-URL GOPHER REMOTE} \
            [string trim $data]
    }

    set-result $token $data
}


proc read-url::protocol::http {token url binary} {
    upvar 1 [namespace parent]::timeout timeout

    http::geturl $url \
        -binary $binary \
        -command [list apply [list {token httpToken} {
            switch -- [http::status $httpToken] {
                eof {
                    set-result \
                        $token \
                        {remote server closed connection without replying} \
                        {-code error -errorcode {READ-URL HTTP EOF}} \
                }
                error {
                    set-result \
                        $token \
                        [http::error $httpToken] \
                        {-code error -errorcode {READ-URL HTTP ERROR}} \
                }
                timeout {
                    set-result \
                        $token \
                        {request timed out} \
                        {-code error -errorcode {READ-URL HTTP TIMEOUT}} \
                }
                default {
                    set ncode [http::ncode $httpToken]
                    set options [expr {
                        $ncode / 100 in {4 5}
                        ? [list \
                            -code error \
                            -errorcode [list READ-URL HTTP CODE $ncode] \
                        ]
                        : {}
                    }]

                    set-result \
                        $token \
                        [http::data $httpToken] \
                        $options \
                }
            }
            http::cleanup $httpToken
        } [namespace current]] $token] \
        -timeout $timeout \
}


proc read-url::parse::gemini-url url {
    if {[regexp \
            {^gemini://([^/:]+)(?::(\d*))?(?:/(.*))?$} \
            $url \
            _ host port path \
        ]} {

        if {$port eq {}} { set port 1965 }

        return [list $host $port $path]
    }

    return {}
}


proc read-url::parse::gopher-url url {
    if {[regexp \
            {^gopher://([^/:]+)(?::(\d*))?(?:/(?:(\d+)/?)?(.*))?$} \
            $url \
            _ host port type path \
        ]} {

        if {$port eq {}} { set port 70 }
        if {$type eq {}} { set type 1 }

        return [list $host $port $type [string trimright $path]]
    }

    return {}
}


proc read-url::tests::find-free-port {myaddr start end} {
    set found false
    for {set port $start} {$port < $end} {incr port} {
        try {
            close [socket -server {} -myaddr $myaddr $port]
        } on error _ continue on ok _ {
            set found true
            break
        }
    }

    if {$found} { return $port }
    error [list couldn't find a port between $start and $end]
}


proc read-url::init-tls {} {
    try {
        package require twapi_crypto
        set read-url::tlsSocketScript [list ::twapi::tls_socket]
    } on error _ {
        package require tls
        set read-url::tlsSocketScript [list ::tls::socket -autoservername true]
    }
    http::register https 443 ${read-url::tlsSocketScript}
}


proc read-url::tests::run {} {
    upvar [namespace parent]::script script

    set scriptDir [file dirname $script]
    set testDataDir [file join $scriptDir test-data]
    lappend ::auto_path [file join $scriptDir .. .. vendor]

    # Set up TLS.
    init-tls

    package require tcltest

    set myaddr 127.0.0.1


    tcltest::test parse::gemini-url-1.1 {} -body {
        parse::gemini-url gemini://example.com
    } -result {example.com 1965 {}}

    tcltest::test parse::gemini-url-1.2 {} -body {
        parse::gemini-url gemini://example.com:2001/
    } -result {example.com 2001 {}}

    tcltest::test parse::gemini-url-1.3 {} -body {
        parse::gemini-url gemini://example.com/~grissom/twtxt.txt
    } -result {example.com 1965 ~grissom/twtxt.txt}

    tcltest::test parse::gemini-url-2.1 {} -body {
        parse::gemini-url {}
    } -result {}

    tcltest::test parse::gemini-url-2.2 {} -body {
        parse::gemini-url ftp://example.com/asdf
    } -result {}

    tcltest::test parse::gemini-url-2.3 {} -body {
        parse::gemini-url gemini://example.com:XY/asdf
    } -result {}


    tcltest::test parse::gopher-url-1.1 {} -body {
        parse::gopher-url gopher://example.com
    } -result {example.com 70 1 {}}

    tcltest::test parse::gopher-url-1.2 {} -body {
        parse::gopher-url gopher://example.com:7070/
    } -result {example.com 7070 1 {}}

    tcltest::test parse::gopher-url-1.3 {} -body {
        parse::gopher-url gopher://example.com/9/games/tronic.zip
    } -result {example.com 70 9 games/tronic.zip}

    tcltest::test parse::gopher-url-2.1 {} -body {
        parse::gopher-url {}
    } -result {}

    tcltest::test parse::gopher-url-2.2 {} -body {
        parse::gopher-url ftp://example.com/asdf
    } -result {}

    tcltest::test parse::gopher-url-2.3 {} -body {
        parse::gopher-url gopher://example.com:XY/!/asdf
    } -result {}


    tcltest::test filesystem-1.1 {text file} -body {
        set token [request [file join $testDataDir test.txt]]
        consume-result $token
    } -match regexp -result {^Many enraged psychiatrists.*"The Policeman's\
                             Beard is Half-Constructed"\s*$}

    tcltest::test filesystem-1.2 {text file over file://} -body {
        consume-result [request file://[file join $testDataDir test.txt]]
    } -match regexp -result {^Many enraged psychiatrists.*"The Policeman's\
                             Beard is Half-Constructed"\s*$}

    tcltest::test filesystem-1.3 {binary file} -body {
        set image [consume-result \
            [request [file join $testDataDir test.png] true] \
        ]

        list \
            [format %X [zlib crc32 $image]] \
            [regexp {^\x89PNG.*\x42\x60\x82$} $image] \
    } -result {E324C084 1}

    tcltest::test filesystem-2.1 {missing file} -body {
        consume-result [request [file join $testDataDir does-not-exist]]
    } -returnCodes error -match glob -result {*does-not-exist*no such file*}

    tcltest::test filesystem-2.2 {no access to file} -constraints unix -body {
        set path [tcltest::makeFile {} no-access $testDataDir]
        file attributes $path -permissions u-r

        consume-result [request $path]
    } -returnCodes error -match glob -result {*permission denied*}


    tcltest::test http-1.1 {example.com} -constraints internet -body {
        consume-result [request https://example.com]
    } -match regexp -result <title>

    set serverPort [find-free-port $myaddr 8080 10000]
    set server [socket -server {apply {{ch clientAddr clientPort} {
        chan event $ch readable [list apply {{ch clientAddr clientPort} {
            chan configure $ch -blocking false -translation binary
            if {[chan blocked $ch]} return

            gets $ch line
            if {[regexp {GET / } $line]} {
                puts $ch "HTTP 1.0 200 OK\r\n\r"
                puts -nonewline $ch "<h1>200 OK</h1>"
            } else {
                puts $ch "HTTP 1.0 404 Not Found\r\n\r"
                puts -nonewline $ch "<h1>404 Not Found!</h1>"
            }
            close $ch
        }} $ch $clientAddr $clientPort]
    }}} $serverPort]
    after-unset server [list close $server]

    tcltest::test http-1.2 {local code 200} -body {
        consume-result [request http://$myaddr:$serverPort/]
    } -result {<h1>200 OK</h1>}

    tcltest::test http-2.1 {nonexistent domain} -constraints internet -body {
        consume-result [request https://does-not-exist.tld]
    } -returnCodes error -match glob -result *

    tcltest::test http-2.2 {local code 404} -body {
        list [catch {
            consume-result [request http://$myaddr:$serverPort/404]
        } e opts] $e [dict get $opts -errorcode]
    } -result {1 {<h1>404 Not Found!</h1>} {READ-URL HTTP CODE 404}}


    set serverPort [find-free-port $myaddr 8080 10000]
    set serverEOF [socket -server {apply {{ch clientAddr clientPort} {
        close $ch
    }}} $serverPort]
    after-unset serverEOF [list close $serverEOF]

    tcltest::test http-2.3 EOF -body {
       list [catch {
            consume-result [request http://$myaddr:$serverPort]
        } _ opts] [dict get $opts -errorcode]
    } -result {1 {READ-URL HTTP EOF}}

    tcltest::test http-2.4 {EOF with TLS} -body {
        list [catch {
            consume-result [request https://$myaddr:$serverPort]
        } e opts] [dict get $opts -errorcode]
    } -result {1 {READ-URL HTTP ERROR}}

    close $serverEOF


    # Exit with a nonzero status if there are failed tests.
    set failed [expr {$tcltest::numTests(Failed) > 0}]

    tcltest::cleanupTests
    if {$failed} {
        return 1
    }

    return 0
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    exit [read-url::tests::run]
}

package provide read-url 0
