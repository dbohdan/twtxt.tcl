#! /usr/bin/env tclsh
# A twtxt parser library for Tcl 8.5+ with partial support for Jim Tcl.
# (The parser working in Jim Tcl depends on whether strptime() in libc
# understands time zones.  In musl libc it does not.)
# Copyright (c) 2020, 2021 D. Bohdan.
# License: MIT.

namespace eval twtxt {
    variable timeRegExp [join {
         {^(\d+)-(\d+)-(\d+)}
         T
         {(\d+):(\d+)((?::\d+)?)((?:\.\d+)?)}
         {((?:Z|[+-]\d+:?\d+)?)$}
    } {}]
    variable version 0.5.0
}

proc twtxt::scan-time time {
    variable timeRegExp
    set scanFormat {%Y %m %d %H %M %S}

    if {![regexp $timeRegExp \
                 $time \
                 _ year month day hour minute second ns tz]} {
        error [list can't scan time: $time doesn't match time regexp]
    }

    # Remove the initial ":".
    set second [string range $second 1 end]
    if {$second eq {}} { set second 00 }

    set timeList [list $year $month $day $hour $minute $second]

    # We use [catch] instead of [try] for Tcl 8.5 compatibility.
    catch {
        if {$tz in {z Z +00 -00 +0000 -0000 +00:00 -00:00}} {
            set t [clock scan $timeList -format $scanFormat -gmt 1]
            return [list $t gmt]
        }

        if {$tz eq {}} {
            set t [clock scan $timeList -format $scanFormat]
            return [list $t local]
        }

        set timeList [concat $timeList $tz]
        set scanFormat [concat $scanFormat %z]
        set t [clock scan $timeList -format $scanFormat]
        return [list $t tz]
    } e opts

    if {[regexp -nocase format $e]} {
        error [list can't scan time: $timeList does not match format \
                    $scanFormat \
        ]
    }

    return {*}$opts $e
}

proc twtxt::parse-feed {feed {mergeSameTimestamp true}} {
    set lines [split [string trimright $feed \n] \n]

    set bad {}
    set metadata {}
    set statuses {}

    set lead true
    set prevTime {}
    foreach line $lines {
        if {[regexp {^\s*$} $line]} continue

        # Metadata is not part of the spec but is found in the wild.
        if {$lead
            && [regexp {^#\s*?([^\s]+?)\s*?=\s*(.*?)\s*$} $line _ key value]} {
            dict lappend metadata $key $value
            continue
        }

        # Comments are also not part of the spec.
        if {[regexp ^# $line]} continue

        set lead false

        if {[catch {
            set i [string first \t $line]
            set time [string range $line 0 $i-1]
            set text [string range $line $i+1 end]

            if {$mergeSameTimestamp && $time == $prevTime} {
                lset statuses end [lindex $statuses end]\n$text
            } else {
                lappend statuses [lindex [scan-time $time] 0] $text
            }

            set prevTime $time
        } e]} {
            lappend bad $line $e
        }
    }

    return [dict create metadata $metadata statuses $statuses bad $bad]
}

proc twtxt::mentions text {
    set all [regexp -all -inline {@<(?:([^> ]+) )? *([^>]*)>} $text ]
    set result {}

    foreach {_ nick url} $all {
        lappend result $nick $url
    }

    return $result
}

proc twtxt::run-tests {} {
    set stats [dict create total 0 passed 0 failed 0]

    proc test {name script arrow expected} {
        upvar stats stats

        dict incr stats total

        catch $script result

        set matched [switch -- $arrow {
            ->   { expr {$result eq $expected} }
            ->*  { string match $expected $result }
            ->$  { regexp -- $expected $result }
            default {
                return -code error \
                       -errorcode {JIMLIB TEST BAD-ARROW} \
                       [list unknown arrow: $arrow]
            }
        }]

        if {!$matched} {
            set error {}
            append error "\n>>>>> $name failed: [list $script]\n"
            append error "      got: [list $result]\n"
            append error " expected: [list $expected]"
            if {$arrow ne {->}} {
                append error "\n    match: $arrow"
            }

            dict incr stats failed

            puts stderr $error
            return
        }

        dict incr stats passed
    }

    test basic-1 {
        parse-feed "2020-07-21T05:51:29.000000Z\tHello, world!"
    } -> {metadata {} statuses {1595310689 {Hello, world!}} bad {}}

    test basic-2 {
        parse-feed "2020-07-21T05:51:29+0100\tHello, world!\n"
    } -> {metadata {} statuses {1595307089 {Hello, world!}} bad {}}

    test basic-3 {
        parse-feed "\n\n1970-01-01T00:00:00Z\tHello...\n# Comment!\
                    \n1970-01-01T00:00:01-0000\t...world!\n"
    } -> {metadata {} statuses {0 Hello... 1 ...world!} bad {}}

    test basic-4 {
        parse-feed "hell\nno"
    } ->$ {metadata \{\} statuses \{\} bad \{hell \{.*\} no \{.*\}\}}

    set t [clock seconds]
    test basic-5 {
        upvar 1 t t
        set format %Y-%m-%dT%H:%M:%S
        parse-feed "[clock format $t -format $format]\tasdf\n#\
                    not = meta\nfail"
    } ->* "metadata {} statuses {$t asdf} bad {*}"

    test basic-6 {
        parse-feed "\n\n\n   \n\t\n"
    } -> {metadata {} statuses {} bad {}}

    test merge-1 {
        set ts 2020-09-23T09:30:00Z
        parse-feed "$ts\tfirst line\n$ts\tsecond line\n"
    } -> "metadata {} statuses {1600853400 {first line\nsecond line}} bad {}"

    test merge-2 {
        set ts 2020-09-23T09:30:00Z
        parse-feed "$ts\tfirst line\n$ts\tsecond line\n" false
    } -> "metadata {} statuses {1600853400 {first line}\
          1600853400 {second line}} bad {}"

    test scan-time-1 {
        scan-time {}
    } -> {can't scan time: {} doesn't match time regexp}

    test scan-time-2 {
        scan-time 1970-01-01T00:00:00Z
    } -> {0 gmt}

    test scan-time-3 {
        scan-time 2015-01-01T00:00:00
    } ->$ {14\d+ local}

    test scan-time-4 {
        scan-time 2015-01-01T00:00:00+01:00
    } ->$ {14\d+ tz}

    test scan-time-5 {
        scan-time 2015-01-01T12:34Z
    } -> {1420115640 gmt}

    test metadata-1 {
        parse-feed "#key = value\n#\tfoo=bar 123"
    } -> {metadata {key value foo {{bar 123}}} statuses {} bad {}}

    test metadata-2 {
        parse-feed "# url =  https://example.com/feed.txt \t \n"
    } -> {metadata {url https://example.com/feed.txt} statuses {} bad {}}

    test metadata-3 {
        parse-feed "# Intro!\n# Yeah!\
                    \n\n#    nick = x\n#     url = y\n#    lang = en\n"
    } -> {metadata {nick x url y lang en} statuses {} bad {}}

    test metadata-4 {
        set s "# following = %s\n"
        parse-feed [format $s foo][format $s bar][format $s baz]
    } -> {metadata {following {foo bar baz}} statuses {} bad {}}

    test mentions-1 {
        mentions {foo @<http://example.com/feed> bar baz!!}
    } -> {{} http://example.com/feed}

    test mentions-2 {
        mentions "@<alef http://example.com/alef.txt>\
                  @<bet http://example.com/bet.txt>"
    } -> {alef http://example.com/alef.txt bet http://example.com/bet.txt}

    test mentions-3 {
        mentions {@<xx yy zz ww> hi}
    } -> {xx {yy zz ww}}

    test mentions-4 {
        mentions @<>
    } -> {{} {}}

    test mentions-5 {
        mentions {@< > @<  > @<   >}
    } -> {{} {} {} {} {} {}}

     puts stderr $stats

    return [expr {[dict get $stats failed] > 0}]
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    exit [twtxt::run-tests]
}

package provide twtxt 0
